#!/bin/bash

. func.sh

if [ x$1 == x ]; then
	echo you need to say the nr of CPUs
	exit 1
fi

if [ $1 -gt `nproc --all` ]; then
	echo too many cpus
	exit 1
fi

if [ $1 -lt 1 ]; then
	echo too few cpus
	exit 1
fi

disable_all_cpus

if [ $1 -eq 1 ]; then
	exit 0;
fi

enable_nr_cpus $(( $1 - 1 ))
