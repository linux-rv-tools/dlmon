# nothing
disable_all_cpus() {
	# do no touch 0!
	for i in /sys/devices/system/cpu/cpu[1-9]*; do
		echo 0 > $i/online;
	done
}

enable_all_cpus() {
	# do no touch 0!
	for i in /sys/devices/system/cpu/cpu[1-9]*; do
		echo 1 > $i/online;
	done
}

# array of CPUs to turn on
enable_some_cpus() {
	for i in "$@"; do
		echo 1 > /sys/devices/system/cpu/cpu${i}/online;
	done
}

enable_nr_cpus() {
	for i in `seq 1 $1`; do
		echo 1 > /sys/devices/system/cpu/cpu${i}/online;
	done
}
