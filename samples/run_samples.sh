#!/bin/bash

if [ ! -d log ]; then
	mkdir log
	if [ ! -d log ]; then
		echo "error creating log dir, odd"
		exit
	fi
fi

# prepare for single core experiments
. scripts/func.sh

disable_all_cpus

#start dlmon
if [ ! -f ../dlmon ]; then
	cd ..
	make > /dev/null
	cd samples
fi

if [ ! -f ../dlmon ]; then
	echo please, compile dlmon
	exit 1
fi

# dispatch dlmon - make it dies after 5 min, just in case the script
# is broken.
timeout 350 ../dlmon & 

# run single core experiments
rt-app rt-app/single.json 2>  log/runtime_log > log/runtime_log
rt-app rt-app/single_core_preempt.json  2> log/runtime_log 1> log/runtime_log
rt-app rt-app/single_5_tasks.json 2> log/runtime_log > log/runtime_log

# re-enable all CPUs
enable_all_cpus

# run multiple core experiments
rt-app rt-app/multi.json 2> log/runtime_log > log/runtime_log

# let the instrumentation collect the data
sleep 3
# done
killall dlmon

rm -rf log
