# Sample workloads

The `dlmon` aims to monitor threads running under SCHED_DEADLINE scheduler. This directory contains some instructions about how to set up the system and generate synthetic workloads that aim causing events.

# Core count

Some of the deadline scheduling properties are only valid for single CPU systems. It is possible to use cpu hotplug functions to simulate this environment or set an arbitrary number of CPUs (as long as the nr is less than or equals the number of real CPUs).

To faciliate, the following scripts are available in the scripts folder:

 - `scripts/single_core.sh`: disable all CPUs but the CPU 0;
 - `scripts/set_nr_cpus.sh`: set an arbitrary number of CPUs, passed as the first argument;
 - `scripts/all_cores.sh`: enable all possible CPUs;

# Using RT-APP to create workloads

rt-app allows the user to create a synthetic workload.

## installing rt-app

To install on fedora:

```
  # dnf install json-c json-c-devel
  # git clone https://github.com/scheduler-tools/rt-app
  # cd rt-app
  # ./autogen.sh
  # ./configure
  # make
  # make install
```

`samples/rt-app/install_fedora.sh` should do all the work for you.

## Running rt-app

To run rt-app you need a config file in the json format. The directory rt-app has some sample setup for rt-app. For example:

 - rt-app/single.json: creates a single thread with 10 ms runtime and 100 ms period parameters, will run for 2 ms in each activation;
 - rt-app/single_core_preempt.json: creates two threads with 1 ms runtime and 3 ms period (will run for 500 us), and one thread with 10 ms runtime and 100 ms period (will run for 8ms). This setup tries to create preemption events;
 - rt-app/single_5_tasks.json: creates five tasks with a runtime of 10 ms, and a period of 100 ms. The task will run for 2 ms.
 - rt-app/multi.json: dispatches 48 threads of 10ms/100ms runtime/period setup, with 2 ms of runtime.

To run the first example, you need to:

```
  # mkdir log [ if a log dir does not exist ]
  # rt-app rt-app/single.json
```
One can edit these files. In a nutshell, in the JSON file, each `task` is a process with `instance` threads. Each thread will receive the `dl-runtime` and `dl-period` priority and will run a loop for `run` time. All times are in microseconds. The timer is the timer that wakes up the task at each `period.`

The tool/experiment will run for `duration` seconds.

# I am lazy
```
 # ./run_samples.sh
```
