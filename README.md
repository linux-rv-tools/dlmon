# dlmon

dlmon is the instrumentation and the collection of scripts used to monitor
threads running under the SCHED_DEADLINE Linux scheduler.

dlmon is an eBPF program that translates kernel events into high level
abstractions used in the real-time systems theory. dlmon aims to serve
as the instrumentation layer to feed runtime monitors that will
analyze the trace using a formal specification of the expected (or
unexpected) behavior of the system.

# Installing

On Fedora, the user needs to install:

	sudo dnf install git glibc-devel glibc-devel.i686 bpftool clang llvm

## Building dlmon

to build dlmon:

	$ make
	# sudo make install

## Author
Daniel Bristot de Oliveira <bristot@kernel.org>
