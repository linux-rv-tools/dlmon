// SPDX-License-Identifier: GPL-2.0

#include "vmlinux.h"
#include <bpf/bpf_helpers.h>
#include "dlmon.h"

struct dl_parameters_t {
	u64 runtime;
	u64 period;
	struct lockdep_map *lock;
};

struct {
	__uint(type, BPF_MAP_TYPE_HASH);
	__type(key, u32);
	__type(value, struct dl_parameters_t);
	__uint(max_entries, 1024);
} dl_param SEC(".maps");

struct {
	__uint(type, BPF_MAP_TYPE_PERF_EVENT_ARRAY);
	__uint(key_size, sizeof(u32));
	__uint(value_size, sizeof(u32));
} events SEC(".maps");

#define BPF_F_CURRENT_CPU 0xffffffffULL

static void update(u64 *ctx, struct task_params *params)
{
	struct dl_parameters_t update = {
		.runtime = params->runtime,
		.period = params->period,
	};

	bpf_map_update_elem(&dl_param, &params->pid, &update, 0); /* 0 is BPF_ANY */
}

static void demote(u64 *ctx, struct task_struct *p)
{

	struct task_params params = {
		.event = EVENT_DEMOTED,
		.runtime = 0,
		.period = 0,
		.pid = p->pid,
	};

	params.now = bpf_ktime_get_ns();

	bpf_perf_event_output(ctx, &events, BPF_F_CURRENT_CPU, &params, sizeof(params));

	update(ctx, &params);
}

static void promote(u64 *ctx, struct task_struct *p, u64 runtime, u64 period, bool wakeup, bool in)

{
	struct task_params params = {
		.runtime = runtime,
		.period = period,
		.pid = p->pid,
	};

	/*
	 * Promote if it is blocked, wake if it ready
	 * but not running yet. Running if it is on_cpu.
	 *
	 * Check kernel/sched/core.c.
	 */
	params.event = EVENT_PROMOTED;
	if (wakeup)
		params.event = EVENT_PROMOTED_WAKE;
	if (in)
		params.event = EVENT_PROMOTED_RUN;

	/*
	 * There is not much space for params, so let's put
	 * the CPU and the event in the same field.
	 */
	params.event = (p->thread_info.cpu * 100) + params.event;
	params.now = bpf_ktime_get_ns();

	bpf_perf_event_output(ctx, &events, BPF_F_CURRENT_CPU, &params, sizeof(params));

	update(ctx, &params);
}


SEC("fentry/__sched_setscheduler")
int handle__sched_setscheduler(u64 *ctx)
{
	/* TP_PROTO(struct task_struct *p, ) */
	struct task_struct *p = (void *) ctx[0];
	struct sched_attr *attr = (void *) ctx[1];
	struct dl_parameters_t *lookup;
	struct dl_parameters_t update;
	struct task_params params;
	u64 runtime, period;
	u32 pid = p->pid;

	/*
	 * Set the event and the runtime.
	 */
	if (attr->sched_policy != SCHED_DEADLINE) {
		lookup = bpf_map_lookup_elem(&dl_param, &pid);

		/*
		 * if we do not know, we do not care.
		 */
		if (!lookup)
			return 0;

		/*
		 * if we know its runtime is 0, we do not care.
		 */
		if (lookup->runtime == 0)
			return 0;

		demote(ctx, p);
		return 0;
	}

	promote(ctx, p, attr->sched_runtime, attr->sched_period, p->on_rq, p->on_cpu);

	return 0;
}

SEC("tp_btf/contention_begin")
int handle__contention_begin(u64 *ctx)
{
	struct lockdep_map *lock = (void *) ctx[0];
	struct dl_parameters_t *lookup;
	u64 pid = bpf_get_current_pid_tgid();
	pid = pid & 0xffffffff;

	lookup = bpf_map_lookup_elem(&dl_param, &pid);
	/* It is leaving so... */
	if (!lookup)
		return 0;

	lookup->lock = lock;

	return 0;
}

SEC("tp_btf/contention_end")
int handle__contention_end(u64 *ctx)
{
	struct dl_parameters_t *lookup;
	u64 pid = bpf_get_current_pid_tgid();

	pid = pid & 0xffffffff;

	lookup = bpf_map_lookup_elem(&dl_param, &pid);
	/* It is leaving so... */
	if (!lookup)
		return 0;

	lookup->lock = 0;
	return 0;
}

SEC("tp_btf/sched_pi_setprio")
int handle__sched_pi_setprio(u64 *ctx)
{
	struct task_struct *boosted = (void *) ctx[0];
	struct task_struct *booster = (void *) ctx[1];
	struct dl_parameters_t *lookup;
	struct task_pi_event pi_event;
	u32 pid;

	if (booster) {
		/*
		 * Nothing to do if the booster is not DL.
		 */
		if (booster->policy != SCHED_DEADLINE)
			return 0;

		pid = booster->pid;

		/* Get the known parameters, ignore if unknown */
		lookup = bpf_map_lookup_elem(&dl_param, &pid);
		if (!lookup)
			return 0;

		pi_event.event = EVENT_BOOST;
		pi_event.booster = booster->pid;
		pi_event.lock = (u64) lookup->lock;
	} else {
		/*
		 * Nothing to do if the boosted is not running as DL.
		 */
		if (boosted->policy != SCHED_DEADLINE)
			return 0;

		pi_event.event = EVENT_DEBOOST;
		pi_event.booster = 0;
	}


	pi_event.boosted = boosted->pid;
	pi_event.on_cpu = boosted->on_cpu;
	pi_event.on_rq = boosted->on_rq;
	pi_event.cpu = boosted->thread_info.cpu;

	pi_event.now = bpf_ktime_get_ns();

	bpf_perf_event_output(ctx, &events, BPF_F_CURRENT_CPU, &pi_event, sizeof(pi_event));

	return 0;
}

SEC("tp_btf/sched_wakeup")
int handle__sched_wakeup(u64 *ctx)
{
	/* TP_PROTO(struct task_struct *p) */
	struct task_struct *p = (void *)ctx[0];
	struct dl_parameters_t *lookup;
	struct dl_parameters_t update;
	struct task_event event = {};
	struct task_params params;
	u32 pid = p->pid;

	if (p->policy != SCHED_DEADLINE)
		return 0;

	/* Get the known parameters, ignore if unknown */
	lookup = bpf_map_lookup_elem(&dl_param, &pid);
	if (!lookup || (lookup->runtime != p->dl.dl_runtime))
		promote(ctx, p, p->dl.dl_runtime, p->dl.dl_period, false, false);

	event.now = bpf_ktime_get_ns();
	event.pid = p->pid;
	event.cpu = p->thread_info.cpu;
	event.event = EVENT_WAKEUP;

	bpf_perf_event_output(ctx, &events, BPF_F_CURRENT_CPU, &event, sizeof(event));

	return 0;
}

SEC("tp_btf/sched_switch")
int handle__sched_switch(u64 *ctx)
{
	/* TP_PROTO(bool preempt, struct task_struct *prev,
	 *	    struct task_struct *next)
	 */
	struct task_struct *prev = (struct task_struct *)ctx[1];
	struct task_struct *next = (struct task_struct *)ctx[2];
	struct task_event event_out = {};
	struct task_event event_in = {};
        struct dl_parameters_t *lookup;
	unsigned int state;
	__u64 now;
	u32 pid;

	state = prev->__state;

	now = bpf_ktime_get_ns();

	if (prev->policy == SCHED_DEADLINE) {
		pid = prev->pid;

		lookup = bpf_map_lookup_elem(&dl_param, &pid);
		/* It is leaving so... */
		if (!lookup || lookup->runtime != prev->dl.dl_runtime)
			return 0;

		event_out.now = now;
		event_out.pid= prev->pid;
		event_out.cpu = prev->thread_info.cpu;

		if (state == 0 || state == 255)
			event_out.event = EVENT_PREEMPT;
		else if (state == 2)
			event_out.event = EVENT_BLOCK;
		else if (state == 16)
			event_out.event = EVENT_EXIT;
		else
			event_out.event = EVENT_SUSPEND;

		bpf_perf_event_output(ctx, &events, BPF_F_CURRENT_CPU,
				      &event_out, sizeof(event_out));

		if (prev->__state == 128)
			demote(ctx, prev);
	}

	if (next->policy == SCHED_DEADLINE) {
		pid = next->pid;

		/* Get the known parameters, if not know, promote it */
		lookup = bpf_map_lookup_elem(&dl_param, &pid);
		if (!lookup || lookup->runtime != next->dl.dl_runtime)
			promote(ctx, next, next->dl.dl_runtime, next->dl.dl_period, true, false);

		event_in.now = now;
		event_in.pid= next->pid;
		event_in.cpu = next->thread_info.cpu;
		event_in.event = EVENT_IN;
		bpf_perf_event_output(ctx, &events, BPF_F_CURRENT_CPU, &event_in, sizeof(event_in));
	}

	return 0;
}

char LICENSE[] SEC("license") = "GPL";
