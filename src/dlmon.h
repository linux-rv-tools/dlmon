// SPDX-License-Identifier: GPL-v2
#ifndef __RUNQSLOWER_H
#define __RUNQSLOWER_H

#define TASK_COMM_LEN 24

enum event_set {
	EVENT_WAKEUP = 0,
	EVENT_IN,
	EVENT_SUSPEND,
	EVENT_PREEMPT,
	EVENT_BLOCK,
	EVENT_PARAMS,
	EVENT_PROMOTED,
	EVENT_PROMOTED_WAKE,
	EVENT_PROMOTED_RUN,
	EVENT_DEMOTED,
	EVENT_BOOST,
	EVENT_DEBOOST,
	EVENT_EXIT,
};

char *event_name[] = 
{
	"wakeup",
	"in",
	"suspended",
	"preempted",
	"blocked",
	"thread",
	"promoted",
	"promoted_wake",
	"promoted_run",
	"demoted",
	"boost",
	"deboost",
	"exit",
}; 

/*
 * From Linux sched.h:
 *
 * Scheduling policies
 */
#define SCHED_NORMAL		0
#define SCHED_FIFO		1
#define SCHED_RR		2
#define SCHED_BATCH		3
/* SCHED_ISO: reserved but not implemented yet */
#define SCHED_IDLE		5
#define SCHED_DEADLINE		6

struct task_params {
	int event;
	pid_t pid;
	__u64 runtime;
	__u64 period;
	__u64 now;
};

struct task_event {
	int event;
	__u64 now;
	pid_t pid;
	int cpu;
};

struct task_pi_event {
	int event;
	int cpu;
	int on_cpu;
	int on_rq;
	pid_t booster;
	pid_t boosted;
	__u64 now;
	__u64 lock;
};

#endif /* __RUNQSLOWER_H */
