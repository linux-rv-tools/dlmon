// SPDX-License-Identifier: GPL-v2
/*
 * Copyright (c) 2021 Daniel Bristot de Oliveira <bristot@kernel.org>
 *
 * This work was developed in collaboration of ETH Zurich Information Security
 * department, and the department of computer science at the University of
 * Copenhagen, more precisely:
 *  - Srdan Krstic <srdan.krstic@inf.ethz.ch>
 *  - Dmitriy Traytel <traytel@di.ku.dk>
 *  - Matthieu Gras <grasm@student.ethz.ch>
 *
 * Based in the runqslower kernel sample.
 */
#include <argp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/resource.h>
#include <time.h>
#include <bpf/libbpf.h>
#include <bpf/bpf.h>
#include "dlmon.h"
#include "dlmon.skel.h"
#include <signal.h>

int should_stop = 0;

static void singal_handler(int signo, siginfo_t *info, void *extra)
{
	should_stop = 1;
}

static void set_sig_handler(void)
{
	struct sigaction action;

	memset(&action, 0, sizeof(action));

	action.sa_flags = SA_SIGINFO;
	action.sa_sigaction = singal_handler;
	sigemptyset(&action.sa_mask);

	if (sigaction(SIGINT, &action, NULL) == -1) {
		printf("error setting SIGINT handler: %s\n", strerror(errno));
		exit(errno);
	}

	action.sa_flags = SA_SIGINFO;
	action.sa_sigaction = singal_handler;

	if (sigaction(SIGTERM, &action, NULL) == -1) {
		printf("error setting SIGTERM handler: %s\n", strerror(errno));
		exit(errno);
	}
}

struct monitor_callbacks {
	void *events;
	void *lost_events;
};

enum monitors {
	MONPOLY_TEXT = 0,
	CPPMON_TEXT = 0,
	MONINORS_MAX
};

struct monitor_callbacks callbacks[MONINORS_MAX];

struct env {
	pid_t pid;
	__u64 min_us;
	bool verbose;
	enum monitors format;
} env = {
	.min_us = 10000,
	.format = MONPOLY_TEXT,
};

const char argp_program_doc[] =
"dlmon		Trace SCHED_DEADLINE thread events and translate\n"
"		them to the format expected by Monpoly and other monitors\n"
""
"USAGE: dlmon [-v]\n"
"\n";

static const struct argp_option opts[] = {
	{ "verbose", 'v', NULL, 0, "Verbose debug output" },
	{ "monpoly", 'M', NULL, 0, "Monpoly text format" },
	{},
};

static error_t parse_arg(int key, char *arg, struct argp_state *state)
{
	switch (key) {
	case 'v':
		env.verbose = true;
		break;
	case 'M':
		env.format = MONPOLY_TEXT;
		break;
	case 'C':
		env.format = CPPMON_TEXT;
		break;
	default:
		return ARGP_ERR_UNKNOWN;
	};

	return 0;
}

int libbpf_print_fn(enum libbpf_print_level level,
		    const char *format, va_list args)
{
	if (!env.verbose)
		return 0;

	return vfprintf(stderr, format, args);
}

static int bump_memlock_rlimit(void)
{
	struct rlimit rlim_new = {
		.rlim_cur	= RLIM_INFINITY,
		.rlim_max	= RLIM_INFINITY,
	};

	return setrlimit(RLIMIT_MEMLOCK, &rlim_new);
}

/*
 * MonPoly monitor textual callbacks
 */
void handle_monpoly(void *ctx, int cpu, void *data, __u32 data_sz)
{
	const struct task_event *e = data;
	const struct task_params *p = data;
	const struct task_pi_event *pi = data;

	int event = e->event % 100;
	int pro_cpu = e->event / 100;

	switch (event) {
	case EVENT_PARAMS:
		printf("@%llu thread(%d,%llu,%llu)\n", p->now, p->pid, p->period, p->runtime);
		break;
	case EVENT_PROMOTED:
		printf("@%llu promoted(%d,%llu,%llu)\n", p->now, p->pid, p->period, p->runtime);
		break;
	case EVENT_PROMOTED_WAKE:
		printf("@%llu promoted(%d,%llu,%llu)\n", p->now, p->pid, p->period, p->runtime);
		printf("@%llu %s(%d,%d)\n", p->now, event_name[EVENT_WAKEUP], p->pid, pro_cpu);
		break;
	case EVENT_PROMOTED_RUN:
		printf("@%llu promoted(%d,%llu,%llu)\n", p->now, p->pid, p->period, p->runtime);
		printf("@%llu %s(%d,%d)\n", p->now, event_name[EVENT_WAKEUP], p->pid, pro_cpu);
		printf("@%llu %s(%d,%d)\n", p->now, event_name[EVENT_IN], p->pid, pro_cpu);
		break;
	case EVENT_DEMOTED:
		printf("@%llu demoted(%d)\n", p->now, p->pid);
		break;
	case EVENT_BOOST:
		printf("@%llu pi_boosted(%d,%d,%llx)\n", p->now, pi->boosted, pi->booster, pi->lock);
		break;
	case EVENT_DEBOOST:
		printf("@%llu pi_deboosted(%d)\n", p->now, pi->boosted);
		break;
	default:
		/* regular events */
		printf("@%llu %s(%d,%d)\n", e->now, event_name[event], e->pid, e->cpu);
	}

	fflush(NULL);
}

void monpoly_lost_events(void *ctx, int cpu, __u64 lost_cnt)
{
	printf("lost %llu events on CPU #%d!\n", lost_cnt, cpu);
}

void register_monitors (void) {

	/* Mompoly output */
	callbacks[MONPOLY_TEXT].events = handle_monpoly;
	callbacks[MONPOLY_TEXT].lost_events = monpoly_lost_events;
	/* CPPMon output */
	callbacks[CPPMON_TEXT].events = handle_monpoly;
	callbacks[CPPMON_TEXT].lost_events = monpoly_lost_events;
}

int main(int argc, char **argv)
{
	static const struct argp argp = {
		.options = opts,
		.parser = parse_arg,
		.doc = argp_program_doc,
	};
	struct perf_buffer *pb = NULL;
	struct dlmon_bpf *obj;
	int retval;
	int err;

	err = argp_parse(&argp, argc, argv, 0, NULL, NULL);
	if (err)
		return err;

	set_sig_handler();
	register_monitors();

	libbpf_set_print(libbpf_print_fn);

	err = bump_memlock_rlimit();
	if (err) {
		fprintf(stderr, "failed to increase rlimit: %d", err);
		return 1;
	}

	obj = dlmon_bpf__open();
	if (!obj) {
		fprintf(stderr, "failed to open and/or load BPF object\n");
		return 1;
	}

	err = dlmon_bpf__load(obj);
	if (err) {
		fprintf(stderr, "failed to load BPF object: %d\n", err);
		goto cleanup;
	}

	err = dlmon_bpf__attach(obj);
	if (err) {
		fprintf(stderr, "failed to attach BPF programs\n");
		goto cleanup;
	}

	pb = perf_buffer__new(bpf_map__fd(obj->maps.events), 64,
				callbacks[env.format].events,
				callbacks[env.format].lost_events,
				NULL, NULL);

	err = libbpf_get_error(pb);
	if (err) {
		pb = NULL;
		fprintf(stderr, "failed to open perf buffer: %d\n", err);
		goto cleanup;
	}

	while (!should_stop) {
		retval = perf_buffer__poll(pb, 100);
		if (retval && !should_stop) {
			/* ignore errors if the user asked to stop */
			printf("Error polling perf buffer: %d\n", err);
			err = retval;
			break;
		}
	};

cleanup:
	perf_buffer__free(pb);
	dlmon_bpf__destroy(obj);

	return err != 0;
}
