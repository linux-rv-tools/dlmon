
#include <linux/ftrace.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/delay.h>
#include <linux/kthread.h>
#include <linux/sched.h>

#define MODULE_NAME "PIT"

#define	debug	trace_printk
#define	err	trace_printk

struct load_params {
	struct spinlock *mutex;
	u64 ns_before;
	u64 ns_during;
	u64 ns_after;
};

#define SCHED_ATTR_SIZE_VER0	48	/* sizeof first published struct */

#define DEADLINE 1
struct sched_attr {
	__u32 size;

	__u32 sched_policy;
	__u64 sched_flags;

	/* SCHED_NORMAL, SCHED_BATCH */
	__s32 sched_nice;

	/* SCHED_FIFO, SCHED_RR */
	__u32 sched_priority;

	/* SCHED_DEADLINE */
	__u64 sched_runtime;
	__u64 sched_deadline;
	__u64 sched_period;
};

struct task_params {
	int started;
	char *comm;
	u64 abs_period;
	struct sched_attr attr;
	struct hrtimer timer;
	struct load_params lparams;
	struct task_struct *task;
};

static int wait_next_period(struct task_params *p)
{
	ktime_t next_abs_period, now;
	u64 rel_period = p->attr.sched_period;
	
	now = hrtimer_cb_get_time(&p->timer);
	next_abs_period = ns_to_ktime(p->abs_period + rel_period);

	debug("%s: suspend: %llu next: %llu\n", p->comm, ktime_to_ns(now),
						ktime_to_ns(next_abs_period));
	/*
	 * Save the next abs_period.
	 */
	p->abs_period = (u64) ktime_to_ns(next_abs_period);

	/*
	 * If the new abs_period is in the past, skip the activation and point to a deadline miss.
	 */
	while (ktime_compare(now, next_abs_period) > 0) {
		err("%s: deadline miss! skipping\n", p->comm);
		next_abs_period = ns_to_ktime(p->abs_period + rel_period);
		p->abs_period = (u64) ktime_to_ns(next_abs_period);
	}

	set_current_state(TASK_INTERRUPTIBLE);

	hrtimer_start(&p->timer, next_abs_period, HRTIMER_MODE_ABS | HRTIMER_MODE_HARD);

	schedule();
	return 1;
}

static int wait_next_aperiod(struct task_params *p)
{
	set_current_state(TASK_INTERRUPTIBLE);
	schedule();
	return 1;
}

static enum hrtimer_restart task_timer(struct hrtimer *timer)
{
	struct task_params *p = container_of(timer, struct task_params,
					     timer);

	debug("%s: arrival now is +- %llu\n", p->comm, p->abs_period);

	wake_up_process(p->task);

	return HRTIMER_NORESTART;
}

static int aperiodic_run(void *param)
{
	struct task_params *p = param;
	struct load_params *l = &p->lparams;

	if(sched_setattr_nocheck(p->task, &p->attr))
		printk("Error setting prio for task %s... continuing anyway\n", p->comm);

	while (!kthread_should_stop()) {

		if (l->ns_before) {
			debug("%s: load before\n", p->comm);
			udelay(l->ns_before / 1000);
		}

		if (l->mutex) {
			debug("%s: taking lock\n", p->comm);
			spin_lock(l->mutex);
			debug("%s: toke lock\n", p->comm);
		}

		if (l->ns_during) {
			debug("%s: load during\n", p->comm);
			udelay(l->ns_during / 1000);
		}

		if (l->mutex) {
			debug("%s: releasing lock\n", p->comm);
			spin_unlock(l->mutex);
		}

		if (l->ns_after) {
			debug("%s: load after\n", p->comm);
			udelay(l->ns_after / 1000);
		}

		wait_next_aperiod(p);
	}

	return 0;
}

void wake_all_aperiodic(void);
static int periodic_run(void *param)
{
	struct task_params *p = param;
	struct load_params *l = &p->lparams;

	if(sched_setattr_nocheck(p->task, &p->attr))
		printk("Error setting prio for task %s... continuing anyway\n", p->comm);

	hrtimer_init(&p->timer, CLOCK_MONOTONIC, HRTIMER_MODE_REL | HRTIMER_MODE_HARD);
	p->timer.function = task_timer;

	/*
	 * Anotate the arrival time.
	 */
	p->abs_period = hrtimer_cb_get_time(&p->timer);

	while (!kthread_should_stop()) {

		debug("my deadline: %llu\n", p->task->dl.deadline);
		if (l->ns_before) {
			debug("%s: load before\n", p->comm);
			udelay(l->ns_before / 1000);
		}

		if (l->mutex) {
			debug("%s: taking lock\n", p->comm);
			spin_lock(l->mutex);
			debug("%s: toke lock\n", p->comm);
		}

		debug("my deadline after lock: %llu\n", p->task->dl.deadline);

		//preempt_disable();
		wake_all_aperiodic();
		debug("my deadline after wakeup: %llu\n", p->task->dl.deadline);
		//preempt_enable();

		if (l->ns_during) {
			debug("%s: load during\n", p->comm);
			udelay(l->ns_during / 1000);
		}

		if (l->mutex) {
			debug("%s: releasing lock\n", p->comm);
			spin_unlock(l->mutex);
		}
		debug("my deadline after unlock: %llu\n", p->task->dl.deadline);

		if (l->ns_after) {
			debug("%s: load after\n", p->comm);
			udelay(l->ns_after / 1000);
		}

		wait_next_period(p);
	}

	hrtimer_cancel(&p->timer);

	return 0;
}

static int create_periodic_task(struct task_params *p)
{
	p->task = kthread_create(periodic_run, p, p->comm);

	if (IS_ERR(p->task)) 
		return 0;

	p->started = 1;

	wake_up_process(p->task);

	return 1;
}

static int create_aperiodic_task(struct task_params *p)
{
	p->task = kthread_create(aperiodic_run, p, p->comm);

	if (IS_ERR(p->task)) 
		return 0;

	p->started = 1;

	wake_up_process(p->task);

	return 1;
}


static void destroy_task(struct task_params *p)
{
	if (p->started)
		kthread_stop(p->task);
}

/*
 * Define the load here
 */
DEFINE_SPINLOCK(pi_mutex);

struct task_params one_par = {
	.comm = "1o10-1",
	.attr = {
		.size = SCHED_ATTR_SIZE_VER0,
#ifdef DEADLINE
		.sched_policy = SCHED_DEADLINE,
		.sched_priority = 0,
		.sched_runtime =    1000000,
		.sched_deadline =  10000000,
		.sched_period  =   10000000
#else
		.sched_policy = SCHED_FIFO,
		.sched_priority = 2,
#endif
	},
	.lparams = {
		.ns_before = 970000,
	}
};

struct task_params two_par = {
	.comm = "1o10-2",
	.attr = {
		.size = SCHED_ATTR_SIZE_VER0,
#ifdef DEADLINE
		.sched_policy = SCHED_DEADLINE,
		.sched_priority = 0,
		.sched_runtime =    1000000,
		.sched_deadline =  10000000,
		.sched_period  =   10000000
#else
		.sched_policy = SCHED_FIFO,
		.sched_priority = 2,
#endif
	},
	.lparams = {
		.ns_before = 970000,
	}
};

struct task_params three_par = {
	.comm = "1o10-3",
	.attr = {
		.size = SCHED_ATTR_SIZE_VER0,
#ifdef DEADLINE
		.sched_policy = SCHED_DEADLINE,
		.sched_priority = 0,
		.sched_runtime =    1000000,
		.sched_deadline =  10000000,
		.sched_period  =   10000000
#else
		.sched_policy = SCHED_FIFO,
		.sched_priority = 2,
#endif
	},
	.lparams = {
		.ns_before = 970000,
	}
};

struct task_params four_par = {
	.comm = "1o10-4",
	.attr = {
#ifdef DEADLINE
		.size = SCHED_ATTR_SIZE_VER0,
		.sched_policy = SCHED_DEADLINE,
		.sched_priority = 0,
		.sched_runtime =    1000000,
		.sched_deadline =  10000000,
		.sched_period  =   10000000
#else
		.sched_policy = SCHED_FIFO,
		.sched_priority = 2,
#endif
	},
	.lparams = {
		.ns_before = 970000,
	}
};

struct task_params five_par = {
	.comm = "1o10-5",
	.attr = {
#ifdef DEADLINE
		.size = SCHED_ATTR_SIZE_VER0,
		.sched_policy = SCHED_DEADLINE,
		.sched_priority = 0,
		.sched_runtime =    1000000,
		.sched_deadline =  10000000,
		.sched_period  =   10000000
#else
		.sched_policy = SCHED_FIFO,
		.sched_priority = 2,
#endif
	},
	.lparams = {
		.ns_before = 970000,
	}
};

struct task_params low_par = {
	.comm = "low",
	.attr = {
#ifdef DEADLINE
		.size = SCHED_ATTR_SIZE_VER0,
		.sched_policy = SCHED_DEADLINE,
		.sched_priority = 0,
		.sched_runtime =    20000000,
		.sched_deadline =  200000000,
		.sched_period  =   200000000
#else
		.sched_policy = SCHED_FIFO,
		.sched_priority = 1,
#endif
	},
	.lparams = {
		.mutex = &pi_mutex,
		.ns_before = 0,
		.ns_during = 2000000,
		.ns_after = 0,
	}
};

struct task_params high_par = {
	.comm = "high",
	.attr = {
		.size = SCHED_ATTR_SIZE_VER0,
#ifdef DEADLINE
		.sched_policy = SCHED_DEADLINE,
		.sched_priority = 0,
		.sched_runtime =     3000000,
		.sched_deadline =    9000000,
		.sched_period  =     9000000
#else
		.sched_policy = SCHED_FIFO,
		.sched_priority = 1,
#endif
	},
	.lparams = {
		.mutex = &pi_mutex,
		.ns_before = 0,
		.ns_during = 1000,
		.ns_after = 0,
	}
};



/*
 * And put it in the list of task.
 */
struct task_params *periodic[] = { &low_par, NULL };
struct task_params *aperiodic[] = { &high_par, &one_par, &two_par,  NULL };

void wake_all_aperiodic(void)
{
	int i;
	struct task_params *p;
        for (i = 0; (p = aperiodic[i]); i++)
		wake_up_process(p->task);
}
#define for_each_task(p) for(p = load[0]; p; p++)

static int __init mod_init(void)
{
	int i;
	struct task_params *p;
	int retval;

	printk(KERN_INFO "Initializing " MODULE_NAME);

	for (i = 0; (p = aperiodic[i]); i++) {
		if (!create_aperiodic_task(p))
			goto out_err;
	}
	
	for (i = 0; (p = periodic[i]); i++) {
		if (!create_periodic_task(p))
			goto out_err;
	}


	return 0;

out_err:
	for (i = 0; (p = periodic[i]); i++)
		destroy_task(p);
	
	for (i = 0; (p = aperiodic[i]); i++)
		destroy_task(p);

	return retval;
}

static void __exit mod_exit(void)
{
	int i;
	struct task_params *p;

	for (i = 0; (p = periodic[i]); i++)
		destroy_task(p);
	for (i = 0; (p = aperiodic[i]); i++)
		destroy_task(p);

	return;
}

module_init(mod_init);
module_exit(mod_exit);

MODULE_LICENSE("GPL v2");
MODULE_AUTHOR("Daniel Bristot de Oliveira");
MODULE_DESCRIPTION("Mutex/PI check");
