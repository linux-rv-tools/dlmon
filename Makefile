STRIP	?=	strip
INSTALL =       install
BINDIR  :=      /usr/bin

all: deps
	make -C src
	mv src/dlmon .
deps:
	@-if [ ! -d libbpf ]; then				\
		git clone https://github.com/libbpf/libbpf.git; \
	fi

install:
	$(STRIP) dlmon
	$(INSTALL) dlmon -m 755 $(DESTDIR)$(BINDIR)

distclean: clean
	rm -rf libbpf
clean:
	rm -rf dlmon src/.output
